GALAXY	:= $(command -v ansible-galaxy || true)

all: deps site

$(GALAXY):
	@curl https://sh.rustup.rs -sSf | sh
	@pip3 install -r requirements.txt
	
key.json:
	@gcloud secrets versions access latest --secret=jenkins-sa-key --project=${PROJECT} > $@
	@gcloud auth activate-service-account --account=${SA_NAME} --key-file= $@
	@gcloud config get-value account

lint:
	@yamllint $(wildcard *.yml)

deps: $(GALAXY) key.json
	@ansible-galaxy install -r requirements.yml

graph: deps
	@ansible-inventory --$@

relias-dev relias-prod relias-staging site: deps
	@ansible-playbook $@.yml
